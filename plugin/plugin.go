package plugin

import (
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/plugin"
)

func Match(path string, info os.FileInfo) (bool, error) {
	switch strings.TrimPrefix(filepath.Ext(info.Name()), ".") {
	case "c", "cc", "cpp", "c++", "cp", "cxx":
		return true, nil
	default:
		return false, nil
	}
}

func init() {
	plugin.Register("flawfinder", Match)
}
