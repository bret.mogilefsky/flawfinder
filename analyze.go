package main

import (
	"bytes"
	"io"
	"os"
	"os/exec"
	"strconv"

	"github.com/urfave/cli"
)

const (
	flagConfidenceLevel = "confidence-level"

	pathOutput     = "/tmp/flawfinder.csv"
	pathFlawfinder = "/usr/local/bin/flawfinder"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		cli.IntFlag{
			Name:   flagConfidenceLevel,
			Usage:  "Confidence level",
			EnvVar: "SAST_FLAWFINDER_LEVEL",
			Value:  1,
		},
	}
}

// ReadCloser wraps a Reader and implements a Close methods that does nothing.
type ReadCloser struct{ io.Reader }

// Close is a fake implementation.
func (r ReadCloser) Close() error {
	return nil
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	level := strconv.Itoa(c.Int(flagConfidenceLevel))
	cmd := exec.Command(pathFlawfinder, "-m", level, "--csv", ".")
	cmd.Dir = path
	cmd.Env = os.Environ()
	cmd.Stderr = os.Stderr

	output, err := cmd.Output()
	if err != nil {
		return nil, err
	}

	return ReadCloser{bytes.NewReader(output)}, nil
}
