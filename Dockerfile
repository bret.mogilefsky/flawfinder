FROM python:latest
RUN pip install flawfinder==2.0.6
COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
